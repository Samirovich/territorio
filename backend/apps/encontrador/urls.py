# coding: utf-8
from django.conf.urls import url

from apps.encontrador.views import LostFriendView
from django.urls import path

app_name = "encontrador:api"

urlpatterns = [
	path('lostfriend', LostFriendView.as_view(), name='lost-friend-view'),
]
