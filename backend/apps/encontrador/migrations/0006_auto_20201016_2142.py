# Generated by Django 3.0.5 on 2020-10-16 21:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('encontrador', '0005_auto_20201016_0219'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='friend',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='contact', to='encontrador.Friend'),
        ),
        migrations.AlterField(
            model_name='friendrace',
            name='friend',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='race', to='encontrador.Friend'),
        ),
        migrations.AlterField(
            model_name='seenlocation',
            name='friend',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='location', to='encontrador.Friend'),
        ),
    ]
