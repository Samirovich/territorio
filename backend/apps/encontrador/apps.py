from django.apps import AppConfig


class EncontradorConfig(AppConfig):
    name = 'encontrador'
