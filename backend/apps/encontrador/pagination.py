# -*- coding: utf-8 -*-
"""Module for handling pagination."""

from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class BasePagination(PageNumberPagination):
    """Base paginator."""
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        """The response(data) will be modfied to be this way."""
        return Response(OrderedDict([
            ('lastPage', self.page.paginator.num_pages),
            ('totalElements', self.page.paginator.count),
            ('countItemsOnPage', len(self.page.object_list)),
            ('current', self.page.number),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('page_size', self.page.paginator.per_page),
            ('contents', data),
         ]))
