# coding: utf-8

"""Mailgun related models."""

from django.db import models
from django.utils import timezone

from django.contrib.postgres.fields import JSONField
from versatileimagefield.fields import VersatileImageField
from django.contrib.gis.db.models import PointField

import uuid


class UUIDMixin(models.Model):
    """Provides uuid field."""
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    class Meta:  # noqa
        abstract = True


class DateMixin(models.Model):
    """
    Provides and handles created date and modified date.
    *it does not use autoadd*
    credits to athanism from:
    https://stackoverflow.com/questions/1737017/django-auto-now-and-auto-now-add/1737078#1737078
    """
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    class Meta:  # noqa
        abstract = True

    def save(self, *args, **kwargs):
        """On save, update timestamps."""
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(DateMixin, self).save(*args, **kwargs)


class Friend(UUIDMixin, DateMixin):
    """Friend model for storing all data related to the lost friend."""
    name = models.CharField(max_length=100)
    GENDERS = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('O', 'Other')
    )
    gender = models.CharField(max_length=1, choices=GENDERS, null=True, blank=True)
    birthdate = models.DateField(null=True, blank=True)
    is_lost = models.BooleanField(default=True)
    has_chip = models.BooleanField(default=False)
    description = models.TextField(null=True, blank=True)


class Species(UUIDMixin, DateMixin):
    """A species."""
    species = models.CharField(max_length=100)


class Race(UUIDMixin, DateMixin):
    """A Race."""
    species = models.ForeignKey('Species', on_delete=models.CASCADE)
    race = models.CharField(max_length=100)


class FriendImage(UUIDMixin, DateMixin):
    """Store an image for the related friend."""
    friend = models.ForeignKey('Friend', on_delete=models.CASCADE, related_name='images')
    image = VersatileImageField(upload_to='images/friends/', blank=True, null=False)


class FriendRace(UUIDMixin, DateMixin):
    """Defines the race of a friend."""
    friend = models.OneToOneField('Friend', on_delete=models.CASCADE, related_name="race")
    race = models.ForeignKey('Race', on_delete=models.CASCADE)


class Contact(UUIDMixin, DateMixin):
    """A relation between friend and human that has contact information."""
    friend = models.ForeignKey('Friend', on_delete=models.CASCADE, related_name="contact")
    person = models.ForeignKey('users.User', on_delete=models.CASCADE)


class SeenLocation(UUIDMixin, DateMixin):
    """A location where a friend was seen."""
    friend = models.ForeignKey('Friend', on_delete=models.CASCADE, related_name="location")
    location = PointField(null=True)


