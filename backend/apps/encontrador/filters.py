# -*- coding: utf-8 -*-

"""Filters for filtering with queryparams."""

from django_filters import rest_framework as filters

from apps.encontrador.models import Friend


class LostFriendFilter(filters.FilterSet):
	"""Filter class for a lost friend..."""
	search = filters.CharFilter(method='search_filter')


class MailgunDataFilter(filters.FilterSet):
	"""Filter class for mailgundata model."""
	search = filters.CharFilter(method='search_filter')
	event = filters.CharFilter(method='event_filter')
	to = filters.CharFilter(method='to_filter')
	_from = filters.CharFilter(field_name='from', method='from_filter')
	sender = filters.CharFilter(method='sender_filter')
	subject = filters.CharFilter(method='subject_filter')
	recipients = filters.CharFilter(method='recipients_filter')


	def event_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				item_data__event=value
			)
		return queryset

	def to_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				storage_data__to=value
			)
		return queryset

	def from_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				storage_data__from=value
			)
		return queryset

	def sender_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				storage_data__sender=value
			)
		return queryset

	def subject_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				storage_data__subject=value
			)
		return queryset


	def recipients_filter(self, queryset, name, value):
		"""We."""
		if value:
			queryset = queryset.filter(
				storage_data__recipients=value
			)
		return queryset

	def search_filter(self, queryset, name, value):
		"""Search filter for searching across different parameters."""
		if value:
			queryset = queryset.filter(
			storage_data__to__icontains=value,
			) | queryset.filter(
			storage_data__from__icontains=value,
			) | queryset.filter(
			storage_data__sender__icontains=value
			) | queryset.filter(
			storage_data__subject__icontains=value
			) | queryset.filter(
			storage_data__recipients__iconatins=value
			) | queryset.filter(
			storage_data__body__iconatins=value
			)
		return queryset.distinct()


	class Meta:  # noqa
		fields = ['event', 'search' ]
		model = MailgunData