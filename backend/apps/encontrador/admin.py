from django.contrib import admin

from apps.encontrador.models import Friend, FriendImage, Species, Contact, Race, FriendRace, SeenLocation


admin.site.register(Friend)
admin.site.register(Species)
admin.site.register(Race)
admin.site.register(FriendImage)
admin.site.register(FriendRace)
admin.site.register(Contact)
admin.site.register(SeenLocation)