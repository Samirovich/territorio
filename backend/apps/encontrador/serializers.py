# coding: utf-8

"""Serializers for mail-gun app."""

from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from apps.encontrador.models import Friend, Species, FriendRace, FriendImage, FriendRace, Contact, SeenLocation
from users.serializers import UserSerializer
from versatileimagefield.serializers import VersatileImageFieldSerializer
from drf_writable_nested.serializers import WritableNestedModelSerializer


class FriendRaceSerializer(serializers.ModelSerializer):
	"""Serializer for friend's race."""
	#TBD: hacer un serializador para raza que no exponga el id y otra info 

	class Meta:
		model = FriendRace
		fields = ('race',)
		depth = 2


class FriendImageSerializer(serializers.ModelSerializer):
	"""Serializer for a friend's image."""
	image = VersatileImageFieldSerializer(sizes=[('full_size', 'url'), ('thumbnail', 'thumbnail__100x100'), ('medium_square_crop', 'crop__400x400'), ('small_square_crop', 'crop__50x50')])

	class Meta:
		model = FriendImage
		fields = ('image',)


class SeenLocationSerializer(GeoFeatureModelSerializer):
	"""Serializer for seen location."""

	class Meta:
		model = SeenLocation
		geo_field = 'location'
		fields = ('uuid', 'location','created')


class ContactSerializer(serializers.ModelSerializer):
	"""Serializer for the contact information of a friend."""
	person = UserSerializer()

	class Meta:
		model = Contact
		fields = ('uuid', 'person')


class LostFriendSerializer(WritableNestedModelSerializer):
	"""A serializer for when someone looses a friend..."""
	race = FriendRaceSerializer()
	images = FriendImageSerializer(many=True)
	contact = ContactSerializer(many=True)
	location = SeenLocationSerializer(many=True)

	class Meta:
		model = Friend
		fields = ('uuid', 'name', 'gender', 'birthdate', 'is_lost', 'has_chip', 'description', 
			'race', 'images', 'contact', 'location')
