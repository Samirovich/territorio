# coding: utf-8

"""Api views for the tracking app."""
from rest_framework.generics import ListCreateAPIView
from apps.encontrador.serializers import LostFriendSerializer
from rest_framework.views import APIView

from apps.encontrador.models import Friend
from apps.encontrador.pagination import BasePagination
from rest_framework.filters import OrderingFilter
from django_filters import rest_framework as filters
from django.views.decorators.csrf import csrf_exempt

class LostFriendView(ListCreateAPIView):
	serializer_class = LostFriendSerializer
	queryset = Friend.objects.all()
	pagination_class = BasePagination
	filter_backends = (filters.DjangoFilterBackend, OrderingFilter,)
	#filter_class = 
	ordering_fields = ('created')
	ordering = ('-created')
