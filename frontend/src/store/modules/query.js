import axios from 'axios'
import Vue from 'vue'

const state = {
  data: {}
}

const getters = {
  getCurrentContent: (state) => (name) => {
    if (state.data[name] && state.data[name].current > 0) {
      let current_page = state.data[name].current
      return state.data[name].pages[current_page].contents
    }
    return []
  },
  getIsLoading: (state) => (name) => {
    if (state.data[name]) {
      return state.data[name].loadingStatus
    }
    return true
  },
  getLastPage: (state) => (name) => {
    if (state.data[name]) {
      return state.data[name].lastPage
    }
    return 0
  },
  getCurrentPage: (state) => (name) => {
    if (state.data[name]) {
      return state.data[name].current
    }
    return 0
  }
}

const mutations = {
  initQueryStore (state, name) {
    let currentStore = state.data[name]
    if (currentStore != undefined) {
      // fixes bugs
      Vue.set(state.data, name, currentStore)
    }
    let newPaginable = {
      empty: true,
      loadingStatus: false,
      pageSize: -1,
      lastPage: -1,
      totalElements: -1,
      current: -1,
      pages: {},
      queryparams: {}
    }
    Vue.set(state.data, name, newPaginable)
    // asd
  },
  appendPage (state, input) {
    let page = input['page']
    let name = input['name']
    const countItemsOnPage = page['countItemsOnPage']
    const contents = page['contents']
    const next = page['next']
    const previous = page['previous']
    if (state.data[name].empty == true) {
      // add the first page
      let new_page = {
        pageSize: page.page_size,
        lastPage: page.lastPage,
        totalElements: page.totalElements,
        current: page.current,
        empty: false,
        pages: {}
      }
      state.data[name] = Object.assign({}, state.data[name], new_page)
    }
    let page_number = page.current
    let appendable_page = {
      countItemsOnPage,
      contents,
      next,
      previous
    }
    state.data[name].pages[page.current] = Object.assign({}, state.data[name].pages[page.current], appendable_page)
  },
  changeCurrent (state, input) {
    let new_current = input['new_page']
    let name = input['name']
    Vue.set(state.data[name], 'current', new_current)
  },
  setLoadingStatus (state, input) {
    let bool = input['bool']
    let name = input['name']
    Vue.set(state.data[name], 'loadingStatus', bool)
  },
  setQueryparams (state, input) {
    let queryparams = input['queryparams']
    let name = input['name']
    Vue.set(state.data[name], 'queryparams', queryparams)
  }
}

const actions = {
  getPage (context, input) {
    let name = input['name']
    let queryparams = input['queryparams']
    context.commit('setLoadingStatus', { name, 'bool': true })
    return new Promise((resolve, reject) => {
      axios.get('/api/encontrador', { params: queryparams }).then(
        (response) => {
          context.commit('setQueryparams', { 'name': name, 'queryparams': queryparams })
          context.commit('appendPage', { 'name': name, 'page': response.data })
          context.commit('setLoadingStatus', { name, 'bool': false })
          resolve()
        }
      )
    }, error => {
      console.log('error')
      reject()
    })
  },
  changePage (context, input) {
    let name = input['name']
    let new_page = input['new_page']
    context.commit('changeCurrent', { name, new_page })
  },
  initStore (context, name) {
    return new Promise((resolve, reject) => {
      context.commit('initQueryStore', name)
      resolve()
    }, error => {
      console.log('error')
      reject()
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
