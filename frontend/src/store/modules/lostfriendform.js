import axios from 'axios'

const state = {
  loading: false,
  error:null,
}

const getters = {
  LostFriendFormIsLoading: (state) => () => {
    return state.loading
  },
  LostFriendFormHasError: (state) => () => {
    if(state.error !== null){
      return true
    }
    return false
  },
  LostFriendFormGetError: (state) => () => {
    return state.error
  },
}

const mutations = {
  LostFriendFormSetLoading (state, bool) {
    state.loading = bool
  },
  LostFriendFormSetError (state, payload) {
    state.error = payload
  }
}

const actions = {
  postLostFriendForm (context, payload) {
    context.commit('LostFriendFormSetLoading', true)
    context.commit('LostFriendFormSetError', null)
    return axios.post('/api/encontrador/lostfriend', payload)
      .then(response => {
        context.commit('LostFriendFormSetLoading', false)
      })
      .catch(e => {
        context.commit('LostFriendFormSetError', e)
        context.commit('LostFriendFormSetLoading', false)
      })
  },
}

export default {
  state,
  getters,
  mutations,
  actions
}
