import Vue from 'vue'
import Vuex from 'vuex'

import users from '@/store/services/users'
import auth from '@/store/modules/auth'
import query from '@/store/modules/query'
import lostfriendform from '@/store/modules/lostfriendform'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
  	//TBD: separar los modulos y ponerles distintos namespaces ( namespaced: true)
    users,
    auth,
    query,
    lostfriendform
  }
})

export default store
