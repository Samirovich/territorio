import Vue from 'vue'
import VueRouter from 'vue-router'

// Components
import Home from './views/Home.vue'
import Search from './views/Search.vue'
import email from './views/email.vue'
import Camera from './views/Camera.vue'
import LostPet from './views/LostPet.vue'

// Plugins
import './plugins'

const routes = [
  {
    path: '/camera',
    name: 'camera',
    component: Camera
  },
  {
    path: '/mascotaperdida',
    name: 'mascotaperdida',
    component: LostPet
  },
  {
    path: '/:search',
    name: 'Search',
    component: Search
  },
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/email',
    name: 'email',
    component: email
  },
  {
	  path: '*',
	  component: Home
  }
]

Vue.use(VueRouter)
const router = new VueRouter({
  scrollBehavior (to, from, savedPosition) { return { x: 0, y: 0 } },
  mode: 'history',
  routes
})

export default router
