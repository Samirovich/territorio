import Vue from 'vue'
import store from '@/store/index'
import router from '@/router'

import axios from 'axios'
// axios.defaults.xsrfCookieName = 'csrftoken'
// axios.defaults.xsrfHeaderName = 'X-CSRFToken'

import Vuetify from 'vuetify'

import App from '@/App.vue'
import './registerServiceWorker'
// leaflet css
import 'leaflet/dist/leaflet.css'
Vue.use(Vuetify)

/*
import VueAnalytics from 'vue-analytics'

import VueRaven from 'vue-raven'
*/
Vue.config.productionTip = false

/*
// Sentry for logging frontend errors
Vue.use(VueRaven, {
  dsn: process.env.VUE_APP_SENTRY_PUBLIC_DSN,
  disableReport: process.env.NODE_ENV === 'development'
})

// more info: https://github.com/MatteoGabriele/vue-analytics
Vue.use(VueAnalytics, {
  id: process.env.VUE_APP_GOOGLE_ANALYTICS,
  router
})
*/

// This adds markers for vue2-leaflet
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
   iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
   iconUrl: require('leaflet/dist/images/marker-icon.png'),
   shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

new Vue({
  vuetify: new Vuetify({ icons: {
    iconfont: 'mdi'
  } }),
  router,
  store,

 	render: h => h(App)
}).$mount('#app')
